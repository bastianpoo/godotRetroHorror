# godotRetroHorror
godot PSX style horror game (...in progress...). basic template created following the instruction of @devlogloga

![imagen](https://user-images.githubusercontent.com/90875843/227679235-8226527e-99e0-4f2e-a7bf-709d53554563.png)

## about

- I get really inspired after seen this result, after spending time studing and dig in tutorials everywhere.

- So, i'll stay with this template, to start a journy in **Game development**.

- Starting with a Trello and simple task to organice my studies and marking short term objetives

this objetives a want to achieve:

- [ ] Enemy tracking
- [ ] More texture
- [ ] Basic horror ambience sounds

### [Here is the starter Trello](https://trello.com/b/J7HZNwSN/proyectpxt).
